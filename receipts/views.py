from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
# Receipt model stuff
@login_required
def home(request):  # list view for the RECEIPT model
    receipt_instances = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_instances": receipt_instances}
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = (
                request.user
            )  # When the receipt is saved, the purchaser must be set to the current user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


# Expense Category Stuff
@login_required
def category_list(request):  # list view for the EXPENSE CATEGORY model
    expense_instance = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expense_instance": expense_instance}
    return render(request, "categories/list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


# Account model stuff
@login_required
def account_list(request):  # list view for different bank / cc accounts
    account_instance = Account.objects.filter(owner=request.user)
    context = {"account_instance": account_instance}
    return render(request, "accounts/list.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "accounts/create.html", context)
